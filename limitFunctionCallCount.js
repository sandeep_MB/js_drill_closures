function limitFunctionCallCount(cb, n) {
    let num = n;
    return function () {
        if (num === 0) {
            return null;
        }
        num--;
        return cb();
    }
}

module.exports = limitFunctionCallCount;