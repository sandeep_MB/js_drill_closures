function counterFactory() {
    let initialValue = 0;

    const increment = function () {
        initialValue++;
        return initialValue;
    }

    const decrement = function () {
        initialValue--;
        return initialValue;
    }

    return { increment: increment, decrement: decrement };
}

module.exports = counterFactory;