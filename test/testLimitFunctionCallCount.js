let limitFunctionCallCount = require("../limitFunctionCallCount.js")

let num1 = 2;
let num2 = 2;
let cb1 = () => num1 = num1 * 2;
let cb2 = () => num2 = num2 * 2;

let n = 4;
let firstFunctionCall = limitFunctionCallCount(cb1, n);
let secondFunctionCall = limitFunctionCallCount(cb2, n);

firstFunctionCall();
firstFunctionCall();

secondFunctionCall();
secondFunctionCall();
secondFunctionCall();
secondFunctionCall();

let actualResult1 = firstFunctionCall();
let actualResult2 = secondFunctionCall();
let expectedResult1 = 16;
let expectedResult2 = null;

if ((actualResult1 === expectedResult1) && (actualResult2 === expectedResult2)) {
    console.log(actualResult1);
    console.log(actualResult2);
}
else {
    console.log("Results are different.");
}