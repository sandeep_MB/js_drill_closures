const counter = require('../counterFactory');

const count = counter();

count.increment();
count.increment();
count.increment();
const actualOutput1 = count.increment();

count.decrement();
count.decrement();
const actualOutput2 = count.decrement();;

const expectedOutput1 = 4;
const expectedOutput2 = 1;

if (actualOutput1 === expectedOutput1 && actualOutput2 === expectedOutput2) {
    console.log(actualOutput1);
    console.log(actualOutput2);
} else {
    console.log('Results are not as expected');
}