let cacheFunction = require('../cacheFunction');

let cb = function (value) { return value * 100 };

let multiplication = cacheFunction(cb);

let actualResult1 = multiplication(7);
let actualResult2 = multiplication(7);

let expectedResult = 700;

if (expectedResult === actualResult1 && actualResult2 === expectedResult) {
    console.log(actualResult1);
} else {
    console.log('Results are Different');
}
