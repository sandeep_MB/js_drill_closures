function cacheFunction(cb) {
    let cache = {};
    const memoizedFunction = (n) => {

        if (n in cache) {
            return cache[n];
        }
        else {
            let result = cb(n);
            cache[n] = result;
            return result;
        }
    }
    return memoizedFunction;
}

module.exports = cacheFunction;
